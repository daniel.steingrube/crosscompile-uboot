#!/bin/sh

UBOOT_VERSION="${CI_UBOOT:=2021.10}"

mkdir uboot
mkdir deploy
D="$(pwd)"

wget http://ftp.denx.de/pub/u-boot/u-boot-${UBOOT_VERSION}.tar.bz2
tar -xjf u-boot-${UBOOT_VERSION}.tar.bz2
cd u-boot-${UBOOT_VERSION} || exit 1

# model a/b/zero
"$D"/patch-uboot.sh rpi_defconfig
make CROSS_COMPILE=arm-linux-gnueabi- distclean
make CROSS_COMPILE=arm-linux-gnueabi- rpi_defconfig
make CROSS_COMPILE=arm-linux-gnueabi- -j8 u-boot.bin
cp u-boot.bin "$D"/uboot/u-boot_rpi1.bin
# model zero w
"$D"/patch-uboot.sh rpi_0_w_defconfig
make CROSS_COMPILE=arm-linux-gnueabi- distclean
make CROSS_COMPILE=arm-linux-gnueabi- rpi_0_w_defconfig
make CROSS_COMPILE=arm-linux-gnueabi- -j8 u-boot.bin
cp u-boot.bin "$D"/uboot/u-boot_rpi0_w.bin
# model 2 b
"$D"/patch-uboot.sh rpi_2_defconfig
make CROSS_COMPILE=arm-linux-gnueabi- distclean
make CROSS_COMPILE=arm-linux-gnueabi- rpi_2_defconfig
make CROSS_COMPILE=arm-linux-gnueabi- -j8 u-boot.bin
cp u-boot.bin "$D"/uboot/u-boot_rpi2.bin
# model 3 (32 bit)
"$D"/patch-uboot.sh rpi_3_32b_defconfig
make CROSS_COMPILE=arm-linux-gnueabi- distclean
make CROSS_COMPILE=arm-linux-gnueabi- rpi_3_32b_defconfig
make CROSS_COMPILE=arm-linux-gnueabi- -j8 u-boot.bin
cp u-boot.bin "$D"/uboot/u-boot_rpi3.bin
# model 4 (32 bit)
"$D"/patch-uboot.sh rpi_4_32b_defconfig
make CROSS_COMPILE=arm-linux-gnueabi- distclean
make CROSS_COMPILE=arm-linux-gnueabi- rpi_4_32b_defconfig
make CROSS_COMPILE=arm-linux-gnueabi- -j8 u-boot.bin
cp u-boot.bin "$D"/uboot/u-boot_rpi4.bin
# 64 bit
"$D"/patch-uboot.sh rpi_arm64_defconfig
make CROSS_COMPILE=aarch64-linux-gnu- distclean
make CROSS_COMPILE=aarch64-linux-gnu- rpi_arm64_defconfig
make CROSS_COMPILE=aarch64-linux-gnu- -j8 u-boot.bin
cp u-boot.bin "$D"/uboot/u-boot_rpi-64.bin

cd "$D"/uboot || exit 1
F="u-boot-noserial-blob-$UBOOT_VERSION.tar.bz2"
tar -cvjf ../deploy/"$F" ./*.bin
cd ../ || exit 1
echo "$F" > deploy/name
echo "$UBOOT_VERSION" > deploy/version
